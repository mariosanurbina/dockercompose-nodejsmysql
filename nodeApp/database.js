const mysql = require('mysql');

let con = mysql.createConnection({
    host: "localhost",
    user: "admin",
    password: "adminPassword",
    database: "appdb",
    host:"dbmysql"
    
  });
  
  con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    var sql = "CREATE TABLE customers (name VARCHAR(255), address VARCHAR(255))";
    con.query(sql, function (err, result) {
      if (err) throw err;
      console.log("Table created");
    });
  });